require "allPositions/version"
require 'uri'
require 'net/http'
require 'xmlrpc/parser'
require "rexml/document"
module AllPositions


  URL_ALL_P="http://allPositions.ru/api/"


  def self.get_projects(url,api_key,id_group=nil)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_projects</methodName><params>"
    if id_group
      str_body+="<param><value><i4>#{id_group}</i4></value></param>"
    end
    str_body+="</params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.get_project(url,api_key,id_project)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_project</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.get_projects_group(url,api_key)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_projects_group</methodName>
            <params></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.get_queries(url,api_key,id_project,id_group=nil)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_queries</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param>"
    if id_group
      str_body+="<param><value><i4>#{id_group}</i4></value></param>"
    end
    str_body+="</params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.get_queries_group(url,api_key,id_project)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_queries_group</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.get_report(url,api_key,id_project,date=nil,prev_date=nil,page=nil,per_page=nil)#name par
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_report</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param>"
    if date and date.class==String
      str_body+="<param><value><string>#{date}</string></value></param>"
    end
    if prev_date and date.class==String
      str_body+="<param><value><string>#{prev_date}</string></value></param>"
    end
    if page and page.class==Fixnum
      str_body+="<param><value><i4>#{page}</i4></value></param>"
    end
    if per_page and per_page.class==Fixnum
      str_body+="<param><value><i4>#{per_page}</i4></value></param>"
    end
    str_body+="</params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def  self.get_report_dates(url,api_key,id_project)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_report_dates</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def  self.get_ses(url,api_key,id_project)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_ses</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def  self.get_visibility(url,api_key,id_project)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>get_visibility</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.delete_queries(url,api_key,*ids)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>delete_queries</methodName><params><param><value><array><data>"
    for i in ids
      str_body+="<value><i4>#{i}</i4></value>"
    end
    str_body+="</data></array></value></param></params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end

  def self.add_queries(url,api_key,id_project,queries,id_group=nil)
    init=init_request(url,api_key)
    request=init["request"]
    http=init["http"]
    str_body="<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>add_queries</methodName><params>
            <param><value><i4>#{id_project}</i4></value></param><param><value><string>#{queries}</string></value></param>"
    if id_group and id_group.class==Fixnum
      str_body+="<param><value><i4>#{id_group}</i4></value></param>"
    end
    str_body+="</params></methodCall>"
    request.body =str_body
    response = http.request(request)
    parse_xml(response.body)
  end


  def self.parse_xml(xml_string)
    parser = XMLRPC::XMLParser::REXMLStreamParser::StreamListener.new
    parser.parse(xml_string)
    parser.instance_variable_get('@value')
  end

  def self.init_request(url,api_key)
    uri = URI.parse("#{url}")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.path)
    request.add_field('Cookie', "api_key=#{api_key}")
    request.add_field('Content-Type', 'text/xml')
    result={}
    result["http"]=http
    result["request"]=request
    result
  end

end

