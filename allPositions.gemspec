# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'allPositions/version'

Gem::Specification.new do |spec|
  spec.name          = "allPositions"
  spec.version       = AllPositions::VERSION
  spec.authors       = ["Igor Kostukovkiy"]
  spec.email         = ["i.kostukovskii@jerminal.com"]
  spec.description   = %q{allPositions.ru api}
  spec.summary       = %q{work with allPositions.ru on rails}
  spec.homepage      = "http://jl.by"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
